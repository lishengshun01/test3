﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public abstract class BaseButtonController : BehaviorController,IPointerUpHandler,IPointerDownHandler,IPointerClickHandler,IPointerEnterHandler,IPointerExitHandler{

	public Image imgButton;

	void Awake()
	{
		imgButton = GetComponent<Image> ();
	}

	public delegate void CallBackClick();

	protected CallBackClick callBackClick;

	public void OnClick(CallBackClick _callBackClick)
	{
		if (_callBackClick != null)
			callBackClick = _callBackClick;
	}

	#region IPointerExitHandler implementation

	public abstract void OnPointerExit (PointerEventData eventData);

	#endregion

	#region IPointerEnterHandler implementation

	public abstract void OnPointerEnter (PointerEventData eventData);

	#endregion

	#region IPointerUpHandler implementation

	public abstract void OnPointerUp (PointerEventData eventData);

	#endregion

	#region IPointerDownHandler implementation

	public abstract void OnPointerDown (PointerEventData eventData);

	#endregion

	#region IPointerClickHandler implementation

	public abstract void OnPointerClick (PointerEventData eventData);

	#endregion
}
