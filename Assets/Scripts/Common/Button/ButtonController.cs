﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : BaseButtonController{

	public override void OnPointerExit (PointerEventData eventData)
	{
		
	}

	public override void OnPointerEnter (PointerEventData eventData)
	{
		
	}

	public override void OnPointerUp (PointerEventData eventData)
	{
		//ScaleUpdate (Vector3.one/0.9f);
	}
		
	public override void OnPointerDown (PointerEventData eventData)
	{
		//ScaleUpdate (Vector3.one/0.8f);
	}
		
	public override void OnPointerClick (PointerEventData eventData)
	{
		if (callBackClick != null) 
			callBackClick();
	}

}
