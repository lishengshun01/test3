﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoapCubeController : MonoBehaviour
{
    Rigidbody rigidbody;
    public bool done = false;
    void Start()
    {
        done = false;
        rigidbody = this.GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!done)
        {
            done = true;
            rigidbody.isKinematic = false;
            transform.rotation = Quaternion.Euler(Random.Range(-10, 11), Random.Range(-10, 11), Random.Range(-10, 11));
            transform.position -= transform.forward / 2;
            if (gameObject.GetComponent<Rigidbody>() == null)
            {
            }
            //Rigidbody rigidbody = collision.gameObject.AddComponent<Rigidbody>();
            Debug.Log(gameObject.name);
            rigidbody.constraints = RigidbodyConstraints.None;
            rigidbody.velocity = -new Vector3(transform.forward.x, transform.forward.y, transform.forward.z) * 5;
            rigidbody.detectCollisions = false;
        }
    }
}
