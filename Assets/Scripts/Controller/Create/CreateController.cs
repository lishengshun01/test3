﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class CreateController : SingletonMonoBehavior<CreateController>
{
    public GameObject Obstacle;
    public PlayerController prefabPlayer;

    public GameObject createObstacle(Vector3 spawnPos,Quaternion rotation,int id)
    {
        GameObject obstacle = GameObject.Instantiate(Obstacle, Vector3.zero, rotation);
        obstacle.transform.localPosition = spawnPos;
        return obstacle;
    }

    public PlayerController SpawnPlayer(Vector3 spawnPos, Quaternion rotation)
    {
        PlayerController playerController = GameObject.Instantiate(prefabPlayer, Vector3.zero, rotation) as PlayerController;
        playerController.transform.localPosition = spawnPos;
        return playerController;
    }

}