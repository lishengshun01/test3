﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
public class MovePattern
{
	public const string Line = "Line";
	public const string Circle = "Circle";
	public const string U_Turn = "U_Turn";
    public const string Curve = "Curve";
	public const string GerenateCurve = "GerenateCurve";
    public const string T_Boss = "T_Boss";
    public const string ECurve = "ECurve";
}

public class Direction
{
	public const string Up = "Up";
	public const string Down = "Down";
	public const string Left = "Left";
	public const string Right = "Right";
	public const string None = "None";
}

public class Point
{
	public Vector3 pos;
	public float timemove;
	public float angle;
	public LeanTweenType leanTweenType;
	public Point (Vector3 _pos, float _timemove,float _angle = 0,LeanTweenType _leanTweenType = LeanTweenType.linear)
	{
		pos = _pos;
		timemove = _timemove;
		angle = _angle;
		leanTweenType = _leanTweenType;
	}
}

public class PointGenerator
{
    public static Vector3 GetMidPoint(Vector3 p1, Vector3 p2)
    {
        return new Vector3((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
    }

    public static List<Vector3> GenerateListPoint()
	{
		List<Vector3> listPoint = new List<Vector3>();




		return listPoint;
	}
	
    private static List<Vector3> CreateListPointLeftRight()
    {
        return null;
    }

	private static List<Vector3> CreateListPoint_Line (Vector3 startPoint, string direction)
	{
		List<Vector3> ListPoint = new List<Vector3> ();

		bool horizontal = false;

		if (direction == Direction.Left || direction == Direction.Right)
			horizontal = true;

		Vector3 newPoint = startPoint;
		float distance = Screen.height / 2 + 200;

		for (int i = 1; i <= 30; i++)
		{
			if (horizontal) 
				newPoint.x += ((direction == Direction.Right) ? distance/30 : -distance/30); 
			else 
				newPoint.y += ((direction == Direction.Up) ? distance/30 : -distance/30);
			
			ListPoint.Add(newPoint);
		}

		return ListPoint;
	}

	//   * * *
	//  *     * 
	// *       *
	// *       *
	// *       *
	//  *     *
	//   * * * 		
	private static List<Vector3> CreateListPoint_Circle (Vector3 StartPoint,Vector3 Center,float Radius, int arcStart, int arcEnd)
	{
		
		List<Vector3> ListPoint = new List<Vector3> ();

		for (int i = arcStart; i <= arcEnd; i+= 10) 
		{
			ListPoint.Add (new Vector3 (
				Center.x + (Radius * (float)Mathf.Cos ((float)i * Mathf.Deg2Rad)),
				Center.y + (Radius * (float)Mathf.Sin ((float)i * Mathf.Deg2Rad))
			));
		}

		return ListPoint;
	}
	//   * * *
	//  *     * 
	// *       *
	// *       *
	// *       *
	//  *     *
	//   * * * 	
	private static List<Point> CreateListPoint_Circle (Vector3 Center,float Radius, int arcStart, int arcEnd)
	{
		if (arcStart > arcEnd)
			arcEnd += 360;
		List<Point> ListPoint = new List<Point> ();
		Vector3 pos;
		for (int i = arcStart; i < arcEnd; i+=20) 
		{
			pos = new Vector3 (
				              Center.x + (Radius * (float)Mathf.Cos ((float)i * Mathf.Deg2Rad)),
				              Center.y + (Radius * (float)Mathf.Sin ((float)i * Mathf.Deg2Rad))
			              );
				
			ListPoint.Add (new Point(pos,0.1f,i+180));
		}
		pos = new Vector3 (
			Center.x + (Radius * (float)Mathf.Cos ((float)arcEnd * Mathf.Deg2Rad)),
			Center.y + (Radius * (float)Mathf.Sin ((float)arcEnd * Mathf.Deg2Rad))
		);

		ListPoint.Add (new Point(pos,1f,arcEnd+180));

		return ListPoint;
	}

	//   * * *
	//  *     * 
	// *       *
	// *       *
	// *       *
	// *       *
	// *       *
	// *       *
	//  *     *
	//   * * * 	
	private static List<Vector3> CreateListPoint_Ellipse(Vector3 Center, float x, float y)
	{
		List<Vector3> listPoint = new List<Vector3> ();

		for (int i = 0; i <= 360; i+=2) {
			listPoint.Add (new Vector3 (
                Center.x + (x * (float)Mathf.Cos((float)(i * Mathf.Deg2Rad))),
                Center.y + (y * (float)Mathf.Sin((float)(i * Mathf.Deg2Rad)))
            ));
        }
        return listPoint;
    }

    public static List<Vector3> CreateListPoint_Ellipse(Vector3 Center, float x, float y,float angle)
    {
        List<Vector3> listPoint = new List<Vector3>();

        for (int i = 0; i <= 360; i+=10)
        {
            Vector3 pos = new Vector3(
                (x * (float)Mathf.Cos((float)(i * Mathf.Deg2Rad))),
                (y * (float)Mathf.Sin((float)(i * Mathf.Deg2Rad)))
            );
            pos = Quaternion.AngleAxis(angle * Mathf.Rad2Deg, Vector3.forward) * pos + Center;

            listPoint.Add(pos);
        }
        return listPoint;
    }

	public static List<Point> CreateListPoint_EllipsePoint(Vector3 Center, float x, float y,float angle)
	{
		List<Point> listPoint = new List<Point>();

		for (int i = 0; i <= 360; i += 10) {
				Vector3 pos = new Vector3 (
					             (x * (float)Mathf.Cos ((float)(i * Mathf.Deg2Rad))),
					             (y * (float)Mathf.Sin ((float)(i * Mathf.Deg2Rad)))
				             );
				pos = Quaternion.AngleAxis (angle * Mathf.Rad2Deg, Vector3.forward) * pos + Center;
				listPoint.Add (new Point (pos, 0.1f));
		}
		for (int i = 0; i < listPoint.Count-1; i++) {
			listPoint [i].angle = Calculate_Angle (listPoint [i+1].pos - listPoint [i].pos)*Mathf.Rad2Deg+90;
			Debug.Log (listPoint [i].angle);
		}
		return listPoint;
	}

	public static List<Point> CreateListPoint_EllipsePointAscend(Vector3 Center,float x, float y, int arcStart = 0, int arcEnd = 360,float angle = 0)
	{
		if (arcStart > arcEnd)
			arcEnd += 360;
		List<Point> listPoint = new List<Point>();

		for (int i = arcStart; i <= arcEnd; i+=10)
		{
			Vector3 pos = new Vector3(
				(x * (float)Mathf.Cos((float)(i * Mathf.Deg2Rad))),
				(y * (float)Mathf.Sin((float)(i * Mathf.Deg2Rad)))
			);
			pos = Quaternion.AngleAxis(angle * Mathf.Rad2Deg, Vector3.forward) * pos + Center;
			listPoint.Add(new Point(pos,0.1f));
		}
		for (int i = 0; i < listPoint.Count-1; i++) {
			listPoint [i].angle = Calculate_Angle (listPoint [i+1].pos - listPoint [i].pos)*Mathf.Rad2Deg+90;
		}
//		listPoint [listPoint.Count-1].angle = Calculate_Angle (listPoint [listPoint.Count-1].pos - listPoint [listPoint.Count-2].pos)*Mathf.Rad2Deg+90;
		return listPoint;
	}

	public static List<Point> CreateListPoint_EllipsePointDescend(Vector3 Center,float x, float y, int arcStart = 0, int arcEnd = 360,float angle = 0)
	{
		if (arcStart > arcEnd)
			arcEnd += 360;
		List<Point> listPoint = new List<Point>();

		for (int i = arcEnd ; i >= arcStart; i-=10)
		{
			Vector3 pos = new Vector3(
				(x * (float)Mathf.Cos((float)(i * Mathf.Deg2Rad))),
				(y * (float)Mathf.Sin((float)(i * Mathf.Deg2Rad)))
			);
			pos = Quaternion.AngleAxis(angle * Mathf.Rad2Deg, Vector3.forward) * pos + Center;
			listPoint.Add(new Point(pos,0.1f));
		}
		for (int i = 0; i < listPoint.Count-1; i++) {
			listPoint [i].angle = Calculate_Angle (listPoint [i+1].pos - listPoint [i].pos)*Mathf.Rad2Deg+90;
		}
//		listPoint [listPoint.Count-1].angle = Calculate_Angle (listPoint [listPoint.Count-1].pos - listPoint [listPoint.Count-2].pos)*Mathf.Rad2Deg+90;
		return listPoint;
	}

    private static List<Vector3> CreateListPoint_FilledRectangle(Vector3 startPoint, int width, int height)
	{
		List<Vector3> listPoint = new List<Vector3> ();
		listPoint.Add (startPoint);

		Vector3 newPoint = startPoint;

		for (int i = 0; i < width * height; i++) 
		{
			if (i % width == 0) 
			{
				newPoint.y -= 100;
				newPoint.x = -100;
			}
			else 
				newPoint.x += 100f;


			listPoint.Add (newPoint);
		}

		return listPoint;
	}

	public static List<Point> CreateListPoint_GerenateCurve(Vector3[] listPoints,float Radius)
	{
		List<Point> result = new List<Point> ();

		int size = listPoints.Length - 1;
		float lastAngle = Calculate_Angle(ref listPoints [size], ref listPoints [0]);
		Create_GerenateCurve (ref listPoints [0], ref listPoints [1], ref result,ref lastAngle,ref Radius);

		for (int i = 1; i < size; i++) {
			Create_GerenateCurve (ref listPoints [i], ref listPoints [i + 1], ref result,ref lastAngle,ref Radius);
		}
			
		Create_GerenateCurve (ref listPoints [size], ref listPoints [0], ref result,ref lastAngle,ref Radius);

		return result;
	}

	public static List<Point> CreateListPoint_GerenateCurveZigzag(Vector3[] listPoints,float Radius)
	{
		List<Point> result = new List<Point> ();

		int size = listPoints.Length - 1;
		float lastAngle = Calculate_Angle(ref listPoints [size], ref listPoints [0]);
		Create_GerenateCurveZigzag (ref listPoints [0], ref listPoints [1], ref result,ref lastAngle,ref Radius,0);

		for (int i = 1; i < size; i++) {
			Create_GerenateCurveZigzag (ref listPoints [i], ref listPoints [i+1], ref result,ref lastAngle,ref Radius,i);
		}

		Create_GerenateCurveZigzag (ref listPoints [size], ref listPoints [0], ref result,ref lastAngle,ref Radius,size);

		return result;
	}

	static void Create_GerenateCurveZigzag(ref Vector3 firstPoint,ref Vector3 lastPoint ,ref List<Point> result,ref float lastAngle ,ref float Radius,int index)
	{
		float Angle = Calculate_AngleZigzag(ref firstPoint,ref lastPoint,index);

		List<Point> listCurvePoints = CreateListPoint_Circle (firstPoint, Radius, (int)(lastAngle * Mathf.Rad2Deg),(int)(Angle * Mathf.Rad2Deg));

		result.AddRange (listCurvePoints);

		lastAngle = Angle;
	}

	static void Create_GerenateCurve(ref Vector3 firstPoint,ref Vector3 lastPoint ,ref List<Point> result,ref float lastAngle ,ref float Radius)
	{
		float Angle = Calculate_Angle(ref firstPoint,ref lastPoint);
		
		List<Point> listCurvePoints = CreateListPoint_Circle (firstPoint, Radius, (int)(lastAngle * Mathf.Rad2Deg),(int)(Angle * Mathf.Rad2Deg));

		result.AddRange (listCurvePoints);

		lastAngle = Angle;
	}

	public static float Calculate_Angle(ref Vector3 firstPoint,ref Vector3 lastPoint)
	{
		Vector3 direction = (lastPoint - firstPoint).normalized;
		direction = new Vector3 (
			direction.y,
			-direction.x
		);
		return Calculate_Angle(direction);
	}

	static float Calculate_AngleZigzag(ref Vector3 firstPoint,ref Vector3 lastPoint,int index)
	{
		Vector3 direction = (lastPoint - firstPoint).normalized;
		if (index % 2 == 0) {
			direction = new Vector3 (
				direction.y,
				-direction.x
			);
			return Calculate_Angle (direction);
		}
		direction = new Vector3 (
			-direction.y,
			direction.x
		);
		return Calculate_Angle (direction);
	}

	static float Calculate_Angle(Vector3 direction)
	{
		float Angle = Mathf.Atan (direction.y / direction.x);

		if (direction.x < 0)
			Angle = Mathf.PI + Angle;
		return Angle;
	}
		

	public static List<Point> CreateListPoint_EllipsePointFrom2Point(Vector3 StartPoint,Vector3 EndPoint,float firstangle,float lastangle,bool isReverse)
	{
		Vector3 direction = EndPoint - StartPoint;
		float Angle = Mathf.Atan(direction.y / direction.x);
		
		if (direction.x < 0)
		   Angle = Mathf.PI + Angle;
		float distanceOf2Pos = Vector3.Distance(StartPoint,EndPoint) / 2;
		Vector3 centerPos = GetMidPoint (StartPoint, EndPoint);

		if ((StartPoint.x > EndPoint.x && isReverse) || (StartPoint.x < EndPoint.x && !isReverse)) 
			{
				return CreateListPoint_EllipsePointAscend (centerPos,distanceOf2Pos,(float)UnityEngine.Random.Range(140, 270),(int)firstangle,(int)lastangle, Angle);
			}
		return CreateListPoint_EllipsePointDescend (centerPos,distanceOf2Pos,(float)UnityEngine.Random.Range(140, 270),(int)lastangle,(int)firstangle, Angle);
	}

    public static List<Point> CreateListPoint_EllipsePointFrom2Points(Vector3 StartPoint, Vector3 EndPoint, float firstangle, float lastangle, bool isReverse)
    {
        Vector3 direction = EndPoint - StartPoint;
        float Angle = Mathf.Atan(direction.y / direction.x);

        if (direction.x < 0)
            Angle = Mathf.PI + Angle;
        float distanceOf2Pos = Vector3.Distance(StartPoint, EndPoint) / 2;
        Vector3 centerPos = GetMidPoint(StartPoint, EndPoint);

        if (!isReverse)
            return CreateListPoint_EllipsePointAscend(centerPos, distanceOf2Pos, (float)UnityEngine.Random.Range(140, 270), (int)firstangle, (int)lastangle, Angle);
        return CreateListPoint_EllipsePointDescend(centerPos, distanceOf2Pos, (float)UnityEngine.Random.Range(140, 270), (int)lastangle, (int)firstangle, Angle);
    }
}
