﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pos
{
	public Point Point;
	public Pos NextPoint;
}

public class PointContainer{

	public static Pos CreateListPoint(List<Point> ListPoint,bool loop)
	{
		int SizeListPoint = ListPoint.Count;
		Pos[] ArrPos = new Pos[SizeListPoint];
		for (int i = 0; i < SizeListPoint; i++) {
			ArrPos [i] = new Pos ();
			ArrPos [i].Point = ListPoint [i];
//			Debug.Log ("CreateListPoint angle Point = " + ListPoint [i].angle);
		}
		for (int i = 0; i < SizeListPoint-1; i++) {
			ArrPos [i].NextPoint = ArrPos [i + 1];
		}
        if(loop)
		ArrPos [SizeListPoint - 1].NextPoint = ArrPos[0];
		return ArrPos [0];
	}

	public static Pos CreateListPoint(List<Vector3> ListPoint,bool loop = true)
	{
		int SizeListPoint = ListPoint.Count;
		Pos[] ArrPos = new Pos[SizeListPoint];
		for (int i = 0; i < SizeListPoint-1; i++) {
			ArrPos [i] = new Pos ();
			ArrPos [i].Point = new Point (ListPoint [i], 0.2f);
		}
		ArrPos [SizeListPoint - 1] = new Pos ();
		ArrPos [SizeListPoint - 1].Point = new Point (ListPoint [SizeListPoint - 1], 0f);
		for (int i = 0; i < SizeListPoint-1; i++) {
			ArrPos [i].NextPoint = ArrPos [i + 1];
		}
		if(loop)
			ArrPos [SizeListPoint - 1].NextPoint = ArrPos[0];
		return ArrPos [0];
	}



    public static Pos CreateListPointWithEase(Vector3 startMovePos,Vector3 endMovePos, Vector3 EaseOffset)
    {
        Pos[] ArrPos = new Pos[6];
        for (int i = 0; i < ArrPos.Length; i++)
        {
            ArrPos[i] = new Pos();
            if (i == 0) ArrPos[i].Point = new Point(startMovePos - EaseOffset, 7);
            if (i == 1) ArrPos[i].Point = new Point(startMovePos, 2.3f);
            if (i == 2) ArrPos[i].Point = new Point(startMovePos - EaseOffset, 2.3f);
            if (i == 3) ArrPos[i].Point = new Point(endMovePos + EaseOffset, 7);
            if (i == 4) ArrPos[i].Point = new Point(endMovePos, 2.3f);
            if (i == 5) ArrPos[i].Point = new Point(endMovePos + EaseOffset, 2.3f);
        }
        for (int i = 0; i < ArrPos.Length - 1; i++)
        {
            ArrPos[i].NextPoint = ArrPos[i + 1];
        }
            ArrPos[ArrPos.Length - 1].NextPoint = ArrPos[0];
        return ArrPos[0];
    }
}
