﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform playerTransform;
    Vector3 offsetVec = new Vector3(0, -3.47f, -25.4f);
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(0, playerTransform.position.y,playerTransform.position.z) + offsetVec;
    }
}
