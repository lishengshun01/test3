﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageInfo
{
    public float[] obstaclePosX;
    public float[] obstaclePosY;
    public float[] obstaclePosZ;
}

public class StageController : SingletonMonoBehavior<StageController>
{
    public GameObject obstacle;
    public GameObject obstacle2;
    public List<GameObject> listObjects = new List<GameObject>();
    void Start()
    {
        //stageInfo = DataController.stageVO.GetArrStage();
        spawnObstacles(11);
        spawnObstacles(31);
        //Instantiate(obstacle2);

    }

    public void spawnObstacles(int startYPos)
    {
        int amount = 20;
        float angle = 360f / amount;
        //int startYPos = 13;
        for(int j = startYPos; j < startYPos+5; j++)
        {
            //GameObject fullObj = new GameObject();
            //fullObj.name = "Target";
            for (int i = 0; i < amount; i++)
            {
                //Instantiate(obstacle, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));
                //GameObject go = Instantiate(obstacle,fullObj.transform) as GameObject;
                GameObject go = Instantiate(obstacle) as GameObject;

                go.transform.Rotate(Vector3.up, angle * i);
                go.transform.position = new Vector3(0,j+0.1f*j,0) - (go.transform.forward * 3);
                go.GetComponent<Rigidbody>().isKinematic = true;
            }
            //fullObj.transform.position = new Vector3(0, j + 0.1f * j);
            //listObjects.Add(fullObj);
        }


        //for (int z = 0; z < 30; z++)
        //{
        //    GameObject go2 = Instantiate(obstacle2) as GameObject;

        //    go2.transform.Rotate(Vector3.up, 360/30 * z);
        //    go2.transform.position = new Vector3(0, 0, 0) - (go2.transform.forward * 4);
        //}


        //Vector3 startPos = new Vector3(0, 5.81f + (4-i), 0);
        //GameObject oj = Instantiate(obstacle, startPos, Quaternion.Euler(0, 0, 0)).gameObject;
    }

    public void setOjActive()
    {

    }

    //public void onReplay()
    //{
    //}
}
