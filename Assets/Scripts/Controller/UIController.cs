﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : SingletonMonoBehavior<UIController>
{
    public GameObject replayBtn;
    public GameObject startBtn;
    // Start is called before the first frame update
    void Start()
    {
        //ManagerController.Instance.ui = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void replayClicked()
    {
        //ManagerController.Instance.onReplay();
        //SceneController.OpenScene("SampleScene");
        SceneManager.LoadScene(0);
    }

    public void playClicked()
    {
        startBtn.SetActive(false);
        ManagerController.Instance.player.done = false;
    }
}
