﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//needs more fix on player movement

public class PlayerController : BehaviorController
{
    public bool done = true;
    public static int currentRow = 1;
    int counter = 0;
    Vector3 startPos = Vector3.zero;
    bool movingLeft = true;
    GameObject[] obstacles;
    public LayerMask collidesWith;
    bool tight=false;
    float lastScale = 1f;
    private void Start()
    {
        done = true;
        lastScale = 1f;
        //MeshRenderer mesh = GetComponent<MeshRenderer>();
        //mesh.bounds.
        //obstacles = StageController.Instance.obstacles;
        ManagerController.Instance.player = this;
        counter = 0;
        currentRow = 1;
        movingLeft = true;
        startPos = transform.position;
        //Physics.gravity = new Vector3(0, -44, 0);
        tight = false;
    }

    public void onUpdatePlayer()
    {
    }

    private void Update()
    {
        //Debug.Log(tight);
        if(!done) //if not dead keep moving
            transform.position += transform.up * Time.deltaTime * 5;

        Vector3 rayStart = new Vector3(transform.position.x - 5, transform.position.y-0.5f*transform.localScale.x, transform.position.z);
        Vector3 rayStart2 = new Vector3(transform.position.x - 5, transform.position.y + 0.5f * transform.localScale.x, transform.position.z);
        RaycastHit hit;
        if (Physics.Raycast(rayStart, transform.right, out hit, 11, collidesWith))
        {
            //Debug.Log(hit.transform.name);
            float scale = 7.142857142857143f / hit.transform.localScale.x;
            if (tight)
            {
                //Debug.Log(hit.transform.localScale.x);
                if (lastScale < scale)
                {
                    ScaleUpdate(new Vector3(1 / scale, 1 / scale, 1 / scale),0.06f);
                }
                //else if (lastScale > scale)
                //{
                //    Debug.Log("ww");
                //    done = true;
                //}
            }
            lastScale = scale;

        }


        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began) //mouse up
            {

                if (tight)
                {
                    tight = false;
                    ScaleUpdate(new Vector3(1, 1, 1));
                    return;
                }
                else
                {
                    RaycastHit hit2;
                    if (Physics.Raycast(rayStart2, transform.right, out hit2, 11, collidesWith))
                    {
                        float scale = 7.142857142857143f / hit2.transform.localScale.x;
                        ScaleUpdate(new Vector3(1 / scale, 1 / scale, 1 / scale));
                        //lastScale = scale;

                    }
                    //ScaleUpdate(new Vector3(1 / lastScale, 1 / lastScale, 1 / lastScale));
                    tight = true;
                    //done = true;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //done = true;
        }
        //Debug.DrawRay(rayStart, transform.right * 11, Color.cyan, 40);
        //Debug.DrawRay(rayStart2, transform.right * 11, Color.red, 40);
    }




    void getMouse()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {

        }
    }

    void getTouch()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began) //mouse up
            {

            }
        }
    }

    IEnumerator afterDelay()
    {
        yield return new WaitForSeconds(1f);
        SceneController.OpenScene("SampleScene");
    }

    private void OnCollisionEnter(Collision collision)
    {
        //transform.position = new Vector3(3, 3, 3);
        if (!done)
        {
            var a = transform;
            if(collision.transform.name == "Cylinder")
            {
                done = true;
                Debug.Log("hit");
                //StartCoroutine(afterDelay());
                UIController.Instance.replayBtn.transform.localScale *= 100;
            }
            if (collision.transform.name == "DeadCube")
            {
                done = true;
                UIController.Instance.replayBtn.transform.localScale *= 100;
                Debug.Log("hit");
            }
        }
        //if(collision.transform.name== "Cube(Clone)")
        //{
        //    Transform cube = collision.transform;
        //    cube.rotation = Quaternion.Euler(Random.Range(-10,11), Random.Range(-10,11), Random.Range(-10, 11));
        //    cube.position -= cube.forward / 2;
        //    if (collision.gameObject.GetComponent<Rigidbody>()==null)
        //    {
        //    }
        //    //Rigidbody rigidbody = collision.gameObject.AddComponent<Rigidbody>();
        //    Debug.Log(collision.gameObject.name);
        //    collision.rigidbody.constraints = RigidbodyConstraints.None;
        //    collision.rigidbody.velocity = -new Vector3(cube.forward.x, cube.forward.y, cube.forward.z) * 5;
        //    collision.rigidbody.detectCollisions = false;
        //    //Debug.DrawRay(collision.transform.position, collision.transform.forward * 3, Color.blue, 40);
        //}
    }
}