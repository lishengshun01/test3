﻿using UnityEngine;
using System;
using System.Collections;

public abstract class BehaviorController : MonoBehaviour
{

    protected Animator animator;
    [SerializeField]
    protected bool invulnerable = false;

    public float moveDuration = 0.1f; //need to ask
    private int id;
    private int idrotation;

    public LeanTweenType __LeanTweenType = LeanTweenType.linear;


    protected void MoveToPos(Vector3 posMoveTo,float moveDuration, Action callBackComplete = null)
    {
        LTDescr ltDescr = LeanTween.moveLocal(gameObject, posMoveTo, moveDuration).setEase(__LeanTweenType);
        id = ltDescr.id;
        ltDescr.setOnComplete(callBackComplete);
    }


    protected void MoveUpdate(Vector3 posMoveTo,float moveDuration)
    {
        LeanTween.cancel(id);
        id = LeanTween.move(gameObject, posMoveTo, moveDuration).setEase(__LeanTweenType).id;
    }

    protected void MoveUpdateToPos(Vector3 posMoveTo,float moveDuration, Action callBackComplete = null)
    {
        LeanTween.cancel(id);
        LTDescr ltDescr = LeanTween.moveLocal(gameObject, posMoveTo, moveDuration).setEase(__LeanTweenType);
        id = ltDescr.id;
        ltDescr.setOnComplete(callBackComplete);
    }

    protected void RotationUpdate(Vector3 rotationTo)
    {
        LeanTween.cancel(idrotation);
        idrotation = LeanTween.rotateLocal(gameObject, rotationTo, moveDuration).setEase(__LeanTweenType).id;
    }
    protected void RotationUpdate(Vector3 rotationTo, float time)
    {
        LeanTween.cancel(idrotation);
        idrotation = LeanTween.rotateLocal(gameObject, rotationTo, time).setEase(__LeanTweenType).id;
    }

    protected void RotationToAngle(Vector3 rotationTo, float time, Action callBackComplete = null)
    {
        LTDescr ltDescr = LeanTween.rotateLocal(gameObject, rotationTo, time).setEase(__LeanTweenType);
        idrotation = ltDescr.id;
        ltDescr.setOnComplete(callBackComplete);
    }

    public void UpdateValue(float firstValue, float lastValue, Action<float, object> updateValue)
    {
        LeanTween.value(gameObject, updateValue, firstValue, lastValue, moveDuration).setEase(__LeanTweenType);
    }

    protected void StopMove()
    {
        LeanTween.cancel(id);
    }

    public void ScaleUpdate(Vector3 ScaleValue)
    {
        LeanTween.cancel(id);
        id = LeanTween.scale(gameObject, ScaleValue, moveDuration).setEase(__LeanTweenType).id;
    }

    public void ScaleUpdate(Vector3 ScaleValue, float moveDuration)
    {
        LeanTween.cancel(id);
        id = LeanTween.scale(gameObject, ScaleValue, moveDuration).setEase(__LeanTweenType).id;
    }

    protected virtual void OnCollision(Collider2D hit)
    {

    }

    protected void DetectCollision()
    {
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, Vector2.zero, Screen.height);
        if (hit.collider != null)
        {
            OnCollision(hit.collider);

        }
    }


}
