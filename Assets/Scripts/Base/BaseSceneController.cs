﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSceneController : SingletonMonoBehavior<BaseSceneController>
{
    public Transform TransCanvas;

    protected virtual void Start()
    {
        TransCanvas = FindObjectOfType<Canvas>().transform;
    }
}
